#!/bin/bash
if [ -z "$1" ];
  then
    echo "Syntax: ./kube-backup-all.sh [cluster_name]"
    exit 1
fi

DATE=$(date +%Y-%m-%d-%H)
export KUBECONFIG=./config
NAMESPACES=`kubectl get ns | awk '{print $1}'  | sed '1d'`
CLUSTER_NAME=$1
RESOURCE_CLASSES="Pods Deployments Secrets DaemonSets StatefulSets ReplicaSets Jobs \
  CronJobs ConfigMaps Secrets ResourceQuotas LimitRanges PriorityClasses HorizontalPodAutoscalers \
  Services Endpoints Ingresses NetworkPolicies PersistentVolumeClaims PersistentVolumes \
  StorageClasses ServiceAccounts ClusterRoles Roles ClusterRoleBindings RoleBindings PodSecurityPolicies"

echo Get namespaces:
rm -rf ./$CLUSTER_NAME/
mkdir -p ./$CLUSTER_NAME/namespaces

for NS in $NAMESPACES
do
echo Get NS $NS
kubectl get ns $NS -o yaml > ./$CLUSTER_NAME/namespaces/$NS.yaml
done

 for NS in $NAMESPACES
 do
 for res in $RESOURCE_CLASSES
 do
 echo Create directory: ./$CLUSTER_NAME/Res/$NS/$res
 echo Get $res in ns $NS
 mkdir -p ./$CLUSTER_NAME/Res/$NS/$res/ # >/dev/null 2>&1
 for resname in $(kubectl get $res -n $NS | awk '{print $1}'  | sed '1d')
 do
 kubectl get $res $resname -n $NS  -o yaml > ./$CLUSTER_NAME/Res/$NS/$res/$resname.yaml
 done
 done
 done
find ./| grep ServiceAccounts/default.yaml |xargs rm -rf {}

tar -cvzpf $CLUSTER_NAME-backup-$DATE.tar.gz ./$CLUSTER_NAME
