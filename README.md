# k8s-res-backup

Скрипт для бэкапа всех ресурсов kubernetes кластера:

```
Pods Deployments Secrets DaemonSets StatefulSets ReplicaSets Jobs \
CronJobs ConfigMaps Secrets ResourceQuotas LimitRanges PriorityClasses HorizontalPodAutoscalers \
Services Endpoints Ingresses NetworkPolicies PersistentVolumeClaims PersistentVolumes \
StorageClasses ServiceAccounts ClusterRoles Roles ClusterRoleBindings RoleBindings PodSecurityPolicies
```