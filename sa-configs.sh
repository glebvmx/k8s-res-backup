#!/bin/bash
if [ -z "$1" ]
  then
    echo "Syntax: ./sa-configs.sh [namespace]"
    exit 1
fi

namespace=$1
clustername=cluster.srelab.net
server=https://kube-master-01.srelab.net:6443
token_name=`kubectl get secrets  -n ${namespace}  | grep ${namespace} |grep token| grep -v ro-token|  awk '{print $1}'`
ca=$(kubectl get secret/${token_name} -o jsonpath='{.data.ca\.crt}' -n ${namespace})
token=$(kubectl get secret/${token_name} -o jsonpath='{.data.token}' -n ${namespace} | base64 --decode)

echo "Generating ./${namespace}.config"

echo "apiVersion: v1
kind: Config
clusters:
  - name: ${namespace}-user@${clustername}
    cluster:
      server: $server
      certificate-authority-data: >-
        $ca
users:
  - name: ${namespace}-user
    user:
      token: >-
        $token
contexts:
  - name: ${namespace}-user@${clustername}
    context:
      user: ${namespace}-user
      cluster: ${namespace}-user@${clustername}
      namespace: $namespace
current-context: ${namespace}-user@${clustername}">${namespace}.config

cat ./${namespace}.config > $HOME/.kube/${namespace}.config